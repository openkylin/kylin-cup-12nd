# 第十二届“麒麟杯”全国开源应用软件开发大赛

## 赛题联系人：
刘晓东：13548656438  
张超：15084896039

## 赛题汇总：

### 桌面功能增强
#### 系统驱动管理模块
赛题说明 :  
基于openKylin操作系统，基于现有“工具箱”应用进行功能增强，实现设备的驱动管理。  
赛题答题要求:  
(1) 展示计算机连接的所有设备，并对异常设备进行识别；  
(2) 提供设备驱动详细信息展示；  
(3) 支持浏览本地驱动程序，并支持更新驱动功能；  
(4) 若设备存在多个驱动，提供驱动切换选项；  
(5) 硬件设备发生变化时，动态扫描并刷新驱动界面。  
赛题指导手册:无  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin/youker-assistant  
2、https://www.gitlink.org.cn/openkylin/youker-assistant  
3、https://www.osredm.com/openkylin/youker-assistant  

#### 桌面高级锁屏模块
赛题说明 :
基于openKylin操作系统，基于现有“锁屏”模块进行功能增强。现有“锁屏”模块缺少与用户交互功能，实现拥有更强交互性的个性化新锁屏显示界面。  
赛题答题要求:  
(1) 锁屏应用被触发后优先启动展示界面，其背景与锁屏界面保持统一，当鼠标点击或者按下键盘任意按钮后退出至锁定界面；  
(2) 展示界面具备可扩展能力，右下角能以插件的形式加载其他组件，如日历、天气、电源状态；  
(3) 展示界面左下角动态的显示时间日期；  
(4) 当系统有音乐在播放时，展示界面提供歌名、歌词的滚动显示，以及上一首、下一首、播放/暂停的选项。  
赛题指导手册:无  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin/ukui-screensaver  
2、https://www.gitlink.org.cn/openkylin/ukui-screensaver  
3、https://www.osredm.com/openkylin/ukui-screensaver  

#### 视觉障碍辅助工具
赛题说明：  
基于openKylin操作系统，实现对视觉障碍用户的桌面使用辅助工具，支持将界面显示的文字转换成语音朗读出来。  
赛题答题要求:  
1、交互设计友好；  
2、支持男声、女生、童声设置，设置界面清晰易懂，功能丰富；  
3、支持中文、英文朗读，语句通顺，断句正常；  
4、提供接口，可供第三方应用程序调用。  
赛题指导手册:  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 无线投屏工具
赛题说明:  
基于openKylin操作系统，实现将桌面无线投屏到另一台openKylin桌面系统上，并在此基础上实现桌面之间增强的交互功能。  
赛题答题要求:  
（1）一台搭载openKylin的PC桌面投射到另一台搭载openKylin的PC，画面、声音同步，延迟在500ms以内；  
（2）设备发现:设备发现不仅仅局限于wifi p2p, 应包含局域网内自动发现，以及根据ip或主机名定向查找等。连接建立成功率不低于90%，除p2p外的建立速度在10s以内；  
（3）硬件共享:在接收端桌面通过鼠标、键盘控制投射端桌面。可通过菜单选择在接收端设备访问投射端设备的麦克风，usb外设  
（4）数据共享:通过拖拽实现两台PC之间的 文件传输。两台PC之间实现剪切板数据共享；  
（5）多设备接入:支持至少两个openKylin桌面设备同时投射到同一个openKylin的接收设备；  
赛题指导手册:  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin/kylin-miraclecast  
2、https://www.gitlink.org.cn/openkylin/kylin-miraclecast  
3、https://www.osredm.com/openkylin/kylin-miraclecast  

### 应用开发
#### 磁盘监控工具
赛题说明:  
基于openKylin操作系统，实现具备图形界面的磁盘监控工具。用于检测磁盘状态，监控硬盘使用情况等。  
赛题答题要求:  
1. 需支持磁盘信息的静态信息检测，包括:  
(1) 硬盘基础信息:磁盘大小、序列号、固件版本、支持协议标准等  
(2) 磁盘当前状态:总读入数据量，总写入数据量，转速（机械硬盘），通电次数，通电时间，当前温度等  
(3) 磁盘分区状态:分区状态，分区表修复  
(4) 磁盘健康状态:坏道检测或坏块检测  
2. 需支持磁盘读写速度检测，包含:  
(1) 磁盘当前IO吞吐量  
(2) 磁盘读写速度检测  
3. 需具备图形界面，可视化展示上述磁盘检测功能，推荐使用QT实现。  
赛题指导手册:linux下相关命令:smartctl ，fdisk;windows下类似工具:crystaldiskinfo  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 磁盘加密工具
赛题说明:  
基于openKylin操作系统，实现通过利用TPM可信根的磁盘设备自动加解密功能，达到计算机设备在丢失或被盗的情况下磁盘数据不被篡改的目标。工具实现过程中数据层加解密使用国家密码局发布的国密算法（SM2/SM3/SM4）。  
赛题答题要求:  
（1）设计实现openKylin操作系统内的块存储设备数据加解密功能，实现加解密块设备、块设备的解锁与锁定、备份密钥、重置密钥、暂停加密、继续加密、开机自动解密和自动加解密等基础功能；  
（2）基于TPM可信根实现自动加解密功能，保证在磁盘丢失或被盗等情形下数据无法被解密或者被篡改；  
（3）实现底层数据加解密的算法定制，使用国密算法；椭圆曲线算法SM2、杂凑算法SM3和分组加解密算法SM4；  
（4）实现“仅加密已用数据区”和“加密全部数据区”；  
（5）附加项:实块设备“在线加密”，块设备使用过程中同时进行加密初始化。  
赛题指导手册:参考开源工具:Cryptsetup、LUKS  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin    

#### 换机克隆工具
赛题说明:  
基于openKylin操作系统，实现从Windows、openKylin将历史数据克隆到新安装的openKylin中。  
赛题答题要求:  
1、界面设计友好，有开始菜单项；  
2、有引导过程，可以设置从哪个机器克隆数据，支持设置本地克隆路径；  
3、支持同步文件属性，如创建日期、访问日期等；  
4、支持显示克隆进度，完成或异常时给出提示；  
赛题指导手册:  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 桌面管理工具
赛题说明:  
基于openKylin操作系统，实现一套可供用户进行桌面文件、应用、图片整理归类的工具  
赛题答题要求:  
（1）工具支持一键桌面整理功能；  
（2）可自定义规则对文件/应用类型进行归类；  
（3）可快速切换多种壁纸，接入扩展屏后不同屏幕可展示不同壁纸；  
（4）支持快捷快速搜索本地文件功能；  
（5）支持设置中心功能，包括设置开机启动桌面自动整理功能等；  
赛题指导手册:  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

### 工具开发
#### 文件签章和打印控制模块
赛题说明:  
基于openKylin操作系统和数科OFD阅读器，1.设计和实现OESV4签章组件，使用国密算法实现计算摘要、签名验签等接口，结合数科OFD阅读器实现盖章验章功能；2.设计和实现OED打印控制组件，结合数科OFD阅读器实现打印控制功能。  
赛题答题要求:  
OESV4签章组件要求:  
（1）根据oesv4.h实现动态库liboes.so;  
（2）摘要算法要求为SM3;  
（3）签名算法要求为SM2；  
（4）电子印章和签章结构符合标准《GB/T 38540-2020 信息安全技术 安全电子签章密码技术规范》要求；  
（5）能够在数科OFD阅读器上实现盖章验章功能。  
OED打印控制组件要求:  
（1） 根据oed.h实现动态库liboed.so；  
（2） 能够在数科阅读器OFD阅读器上设置文档打印份数，超过打印份数后是否运行打印签章、印章黑色打印等参数；  
（3） 能够在数科阅读器OFD阅读器打印OFD文档时控制打印份数，超过打印份数时按以下三种情况处理:  
    1.不允许打印文档；  
    2.允许打印文档不允许打印签章；  
    3.允许打印文档和签章，签章打印为黑色。    
赛题指导手册:  
1. oesv4头文件   
2. oed头文件   
3. liboes.so安装路径为:阅读器安装目录/plugins/sealV4/xx/liboes.so  
4. liboed.so安装路径为:阅读器安装目录/plugins/delegate/xx/liboed.so  
5. 标准《GB/T 38540-2020 信息安全技术 安全电子签章密码技术规范》  
网址:http://c.gb688.cn/bzgk/gb/showGb?type=online&hcno=EBF1360C272E40E7A8B9B32ED0724AB1  
仓库代码上传:需要把学生开发所需代码上传至如下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin  

#### 系统安全分析工具
赛题说明:  
题目共分2个子题目，参赛者需要依次完成题目。完成数目越多，质量越高，最终得分越高。  
题目1:实现openKylin操作系统安全配置基线检查功能生成可视化报告，并能够对基线项给出对应的修复说明。  
题目2:实现openKylin操作系统安全漏洞（POC/EXP）检查功能生成可视化报告，并能够准确分析出当前操作系统中存在的已公开安全漏洞。  

赛题答题要求:  
题目1:  
功能一:实现系统安全配置基线检查:60:检查项覆盖操作系统通用安全需求(参考SCAP标准/等保安全配置基线标准),需生成可视化报告  
功能二:安全基线检查给出修复建议:80:需生成可视化报告  
功能三:实现基线检查项可配置:90:每项检测项可供配置，可根据配置文件/参数进行检查配置  
功能四:实现模块化、跨平台:100:检查功能要求模块化，可通过命令行、命令交互或配置文件进行插拔使用，工具能够跨平台  
题目2:  
功能一:实现安全漏洞（POC/EXP）检查功能:60:检查功能要求模块化，基于安全漏洞poc/exp检测  
功能二:给出对应的系统安全漏洞的修复建议:80:需生成可视化报告  
功能三:工具误报率小于10%:90:要求工具检测安全漏洞的准确率要高于90%  
功能四:集成有效系统安全漏洞poc/exp数量达到200+:100:poc/exp能够通过模块化添加  
赛题指导手册:请参考:https://gitee.com/a-alpha/open-kylin-system-security-analysis-tool  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 系统软件包问题分析工具
赛题说明:  
基于openKylin操作系统，当安装软件包遇到依赖冲突时，会进行提示，但是有时候依赖错误发生在较高的深度时，无法准确提示依赖冲突的根源。希望实现deb包深层依赖冲突的问题分析。  
赛题答题要求:  
（1）完成apt安装软件包时提示完整依赖冲突的功能，完成相关代码和文档；  
（2）修改apt源码中关于依赖冲突的单元测试并执行，验证能够输出完整依赖冲突；  
赛题指导手册:https://gitee.com/openkylin/apt/blob/openkylin/yangtze/README.md  
仓库代码上传:从https://gitee.com/openkylin/apt仓库fork出一份到个人仓库进行开发。  

#### 安全c库函数开发
赛题说明:  
基于openKylin开源软件仓库safeclib，根据C11标准附录K的要求，完善其中的安全函数。  
赛题答题要求:  
参赛者需要依次完成指定安全函数，要求安全函数功能测试结果符合相关标准，完成安全函数数目越多，质量越高，最终得分越高。  
（1）实现以下安全c库函数fscanf_s/scanf_s/sscanf_s/vfscanf_s/vscanf_s/vsscanf_s，要求函数实现功能严格遵守C11标准附录K中该安全函数的相关要求。  
（2）进一步实现以下安全c库函数fwscanf_s/swscanf_s/vfwscanf_s/vswscanf_s/vwscanf_s/wscanf_s要求函数实现功能在对格式化字符串长度识别，缓冲区长度检查等功能跟（1）中函数要求相同。  
赛题指导手册:https://gitee.com/openkylin/sdl-security-develop/blob/master/developers_guide_of_safeclib.md  
仓库代码上传:https://gitee.com/openkylin/safeclib.git  

#### 桌面主题DIY工具
赛题说明:  
基于openKylin操作系统，制作系统主题DIY开发工具，可通过工具生成主题并应用至系统中  
赛题答题要求:  
（1）制作主题开发工具，通过素材上传自动制作主题包，素材可包含图片、音频等；  
（2）制作的主题可通过工具直接应用至系统中，但不删减系统原有主题  
赛题指导手册:  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 手写笔记软件
赛题说明:  
基于openKylin操作系统，开发一款手写笔记应用。能够支持通过压感手写笔进行文字输入，支持不同笔尖粗细、颜色和字体的选择；支持导入图片；同时也支持导入pdf文件进行手写批注。  
赛题答题要求:  
基础项:  
（1）支持通过压感手写笔输入；  
（2）支持不同笔记粗细选择；  
（3）支持不同笔记颜色选择；  
（4）支持不同字体选择；  
（5）支持图片导入；  
加分项:  
（6）支持导入pdf文件进行手写批注；  
赛题指导手册:  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin    

### 系统基础功能
#### 基于wayland的系统图形合成器
赛题说明:  
基于openKylin操作系统，设计、实现基于wlroots的wayland合成器，实现基本的wayland协议，能够正常启动UKUI桌面环境，使用户有良好的交互体验。在基于openKylin操作系统中，设计并实现基于wlroots的wayland合成器。该合成器能够实现基本的wayland协议，且能够顺利启动UKUI桌面环境，为用户提供良好的交互体验。  
赛题答题要求:  
（1）该wayland合成器能够稳定运行UKUI桌面环境，用户能够在这个环境下正常地进行各类操作。  
（2）在空闲状态下，该合成器能够达到60fps的帧率，为用户提供流畅、稳定的操作体验。  
（3）合成器还实现了可扩展的窗口装饰及相关特效，这使得用户能够根据自己的需要自定义窗口装饰，获得更加个性化的体验。  
（4）除此之外，该合成器还支持xwayland，能够兼容历史X应用，这提高了用户体验的全面性和兼容性。  
赛题指导手册:https://gitlab.freedesktop.org/wlroots/wlroots  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 桌面控件主题模块
赛题说明:  
基于openKylin操作系统，实现一个qt的QStyle，可以解析ukui design token导出的json文件（可以参考semi design），并且应用到qt应用的控件样式  
赛题答题要求:  
（1）创建一个新的qtstyle插件ukui-dt，可以解析组件的token并且进行绘制  
（2）允许动态切换已有token改变组件样式  
（3）支持一些高级的属性token，比如动画属性、阴影属性等  
（4）绘制接口实现合理，保留一定的扩展性，且兼顾效率  
赛题指导手册:  
semi design :https://semi.design/zh-CN/start/getting-started  
qtstyle:https://doc.qt.io/qt-5/style-reference.html  
仓库代码上传:  
1、https://gitee.com/openkylin/qt5-ukui-platformtheme  
2、https://www.gitlink.org.cn/openkylin/qt5-ukui-platformtheme  
3、https://www.osredm.com/openkylin/qt5-ukui-platformtheme  

#### 系统无线热点模块
赛题说明:  
基于openkylin操作系统，设计和实无线网卡在连接无线网络的同时并可以创建网络热点功能。  
赛题答题要求:  
（1）基于wpa_suplicant(hostapd)实现  
（2）热点支持IP段、网关的自定义设置功能  
（3）热点支持对接入设备的流量统计功能  
（4）热点支持对接入设备的网速监测功能  
（5）热点支持2.4G、5G、自动频段，三种AP频段选择功能  
（6）热点支持节能模式，开启后一定时间内未被使用，可自动关闭  
（7）热点支持对接入设备的基本信息显示功能（基本信息:设备名称，IP，MAC）  
（8）实现方式不可以通过配置文件、脚本或命令行方式，需提供UI软件  
赛题指导手册:无  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### ELF文件签名验证工具
赛题说明:  
基于RISC-V架构下的openKylin操作系统，采用系统支持的加密散列算法，设计实现一种ELF文件签名验证的机制。  
赛题答题要求:  
技术要求:  
（1）熟悉ELF文件结构，Hook内核函数的方式；  
（2）掌握签名工具的运用，Linux下C编程；  
（3）熟悉加解密流程，了解加解密算法定义；  
产出要求:  
（1）该签名验证方式可以应用在RISC-V架构下openKylin系统；  
（2）对于验证成功和异常的ELF文件有不同的处理措施；  
（3）使用合适的hook内核函数的方式，开销尽可能小；  
（4）输出相应说明文档，阐述实现逻辑；  
赛题指导手册:无  
仓库代码上传:需要把学生开发所需代码上传至如下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin  

### 人工智能  
#### 基于向量搜索的语义检索工具  
赛题说明:  
基于openKylin操作系统，设计和实现基于向量搜索引擎的语义检索工具，在UKUI桌面环境中实现通过文字输入搜索到图片，文本文件等内容的功能。  
赛题答题要求:  
（1）语义搜索支持中英文输入，且至少支持搜索文本文件和图片两种内容。  
（2）在保证精度足够的情况下，通过减少模型参数或向量维度等方案提升向量生成效率，单线程模式下，向量数据库构建峰值内存占用不超过500MB。  
（3）向量数据库支持增量更新。  
（4）召回率达到95%以上。  
（5）搜索支持相关性排序，且单词搜索效率达到100ms以内，且内存峰值占用不超过200MB。  
赛题指导手册:  
可参考ukui-search项目https://gitee.com/openkylin/ukui-search，并基于ukui-search提供的文件扫描监听和索引控制模块实现。  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 手写和语音识别输入模块
赛题说明:  
基于openKylin操作系统，基于开源或商业的手写识别引擎和语音识别引擎为麒麟虚拟键盘增加手写和语音识别功能。该功能使得用户通过可以通过鼠标、触控板和触摸屏等设备进行手写输入和语音输入，为用户提供额外的文字输入方式，提供用户的输入效率。  
赛题答题要求:  
基础项:  
（1）手写识别方案能支持中文或英文手写输入；  
（2）手写识别方案应该向用户提供识别结果选择功能；  
（3）语音识别方案能支持中文或英文语音输入；  
（4）语音识别方案应该将语音识别结果自动输入到可输入区域；  
加分项:  
（5）AI图片生成方案可以根据用户输入向用户提供图片生成功能；  
赛题指导手册:  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 操作系统基础功能AI智能化
赛题说明:  
基于openKylin操作系统，利用现有各种流行的AI技术能力，将其创新的与桌面操作系统的基本功能结合在一起，为桌面操作系统的日常使用赋予AI智能。  
赛题答题要求:  
（1）为操作系统赋予的AI能力新颖有创意  
（2）引入AI后系统功能变得更加简洁高效  
（3）改善或颠覆操作系统交互体验  
赛题指导手册:无  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 基于AI的实况文本功能
赛题说明:  
基于openkylin操作系统和AI相关技术，实现对图片、视频以及在线图像中文本的自动识别、按需求选择以及复制等功能。  
赛题答题要求:  
（1）覆盖图片、视频以及在线图像三种对象；  
（2）对文本的识别速度在1秒以内，用户体验无延时感；  
（3）对文本的自动识别不受限于字体，支持手写体识别；  
（4）支持对文本的划范围选择；  
（5）支持对所选择的文本复制到其他应用；  
赛题指导手册:无  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 基于大模型的办公文档自动生成功能
赛题说明:  
基于openkylin操作系统和Libreoffice开源办公软件，利用最新生成式AI模型，实现按照需求自动生成Office文件等功能。  
赛题答题要求:  
（1）覆盖doc、ppt、excel等三种不同office文件；  
（2）具备自动生成文档能力；  
（3）具备上下文感知能力，可通过多次会话迭代更新；  
（4）基于openKylin操作系统可编译可运行；  
赛题指导手册:无  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

### 区块链
#### 基于IPFS的分布式学习辅助平台
赛题说明:  
基于openKylin操作系统和IPFS星际文件系统构建面向高校学生的学习辅助平台。  
赛题答题要求:  
（1）构建区块链模式下的存储DHTs，开发满足高校学生的辅助学习需求的应用软件，包括课件与资料共享、考试交流等；  
（2）可以以班级、年级或专业为单位进行分享。  
赛题指导手册:https://github.com/ipfs/ipfs  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

#### 基于Hyperledger Fabric的桌面助手软件
赛题说明:  
基于openKylin操作系统和Hyperledger Fabric区块链分布式账本开发桌面辅助工具  
赛题答题要求:  
本题为半开放命题，参赛队应用开源的区块链分布式账本技术，解决个人或企业在日常生活中需要使用的共识算法、加密安全、数字资产、智能合约或身份鉴权等服务（不少于2个）即可。  
赛题指导手册:https://github.com/hyperledger/fabric  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

### AR/VR
#### 基于AR/VR开源工具的虚拟现实应用
赛题说明:  
基于AR/VR开源工具ARToolKit、AR.js、OpenHMD、VRPN和Blender XR等，在openKylin操作系统上开发一个能够运行的AR/VR应用。  
赛题答题要求:  
（1）将AR/VR开源工具移植到openKylin操作系统上；  
（2）基于AR/VR开源工具，开发一个应用程序；  
（3）应用程序可连接可穿戴设备，进行AR/VR体验。  
赛题指导手册:无  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   

### 大数据
#### 用户软件使用习惯分析工具
赛题名称:  
用户软件使用习惯分析工具  
赛题说明:  
基于系统信息收集和大数据分析的方法，实现openKylin操作系统用户的使用习惯统计分析。  
赛题答题要求:  
（1）应用程序和系统崩溃日志:当应用程序或系统崩溃时自动收集相关日志，并向openKylin社区发送匿名的错误报告以便进行调试和改进。     
（2）应用程序使用情况:记录应用程序的启动次数、使用时长、占用系统资源等信息，帮助了解用户对应用程序的使用情况，从而优化应用程序的功能和性能。  
（3）系统使用情况:记录用户的系统使用情况，比如电池电量、CPU、内存、硬盘使用情况等，帮助用户了解自己的设备状态，也可以帮助开发者改进系统性能和功能。  
赛题指导手册:无  
仓库代码上传，以下任意一个平台:  
1、https://gitee.com/openkylin  
2、https://www.gitlink.org.cn/openkylin  
3、https://www.osredm.com/openkylin   
